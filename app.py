import ctypes

lib = ctypes.CDLL("out/libarray.so")

_arr_fill_prime = lib.arr_fill_prime
_arr_fill_prime.argtypes = (ctypes.POINTER(ctypes.c_int), ctypes.c_int)
_arr_fill_prime.restype = None

_arr_form_by_even = lib.arr_form_by_even
_arr_form_by_even.argtypes = (
    ctypes.POINTER(ctypes.c_int), ctypes.c_int, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int),
    ctypes.c_int)
_arr_form_by_even.restype = ctypes.c_int


def arr_fill_prime(arr):
    arr_len = len(arr)
    arr_c = (ctypes.c_int * arr_len)(*arr)

    _arr_fill_prime(arr_c, arr_len)

    return list(arr_c)


def arr_form_by_even_normal_size(arr, num):
    len_arr = len(arr)
    arr_c = (ctypes.c_int * len_arr)(*arr)
    new_arr_len_c = ctypes.c_int(0)
    rc = _arr_form_by_even(arr_c, len_arr, None, new_arr_len_c, num)
    if rc:
        new_arr_c = (ctypes.c_int * new_arr_len_c.value)()
        rc = _arr_form_by_even(arr_c, len_arr, new_arr_c, new_arr_len_c, num)
        return rc, list(new_arr_c)
    return rc, list()


def arr_form_by_even_max_size(arr, num):
    len_arr = len(arr)
    arr_c = (ctypes.c_int * len_arr)(*arr)

    new_arr_c = (ctypes.c_int * (len_arr * 2 + 1))()
    new_arr_len_c = ctypes.c_int(len_arr * 2 + 1)

    rc = _arr_form_by_even(arr_c, len_arr, new_arr_c, new_arr_len_c, num)
    if rc != 0:
        print("Error in arr_form_by_even_max_size!")
    return new_arr_len_c.value, list(new_arr_c)

command = -1
while command != 4:
    print(
        "1 - Заполнить массив простыми числами\n2 - Заполнить массив добавляя число после каждого четного(1)\n3 - Заполнить массив добавляя число после каждого четного(2)\nДругое - Выход")
    command = int(input("Введите пункт: "))

    if command == 1:
        arr_len = int(input("Введите длину массива: "))
        arr = [0] * arr_len
        print(*arr_fill_prime(arr))
    elif command == 2:
        arr = list(map(int, input("Введите массив: ").split()))
        num = int(input("Введите число для вставки: "))
        rc, new_arr = arr_form_by_even_normal_size(arr, num)
        print(*new_arr)
    elif command == 3:
        arr = list(map(int, input("Введите массив: ").split()))
        num = int(input("Введите число для вставки: "))
        new_arr_len, new_arr = arr_form_by_even_max_size(arr, num)
        for i in range(new_arr_len):
            print(new_arr[i], end=" ")
        print()
    else:
        break
