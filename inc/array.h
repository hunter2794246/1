#ifndef __ARRAY_H__
#define __ARRAY_H__

void arr_fill_prime(int *arr, int n);

int arr_form_by_even(int *arr, int len_arr, int *new_arr, int *new_arr_len, int num);

#endif  // __ARRAY_H__
