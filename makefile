CC := gcc

CFLAGS := -std=c99 -Wall -Werror -Wextra
.PHONY : clean

OBJ_PATH := out
SRC_PATH := src/
INC_PATH := inc/
mkdir -p $(OBJ_PATH)


$(OBJ_PATH)/libarray.so: $(SRC_PATH)array.c $(INC_PATH)*.h
	$(CC) $(CFLAGS) -fPIC -I $(INC_PATH) -o $(patsubst %.so, %.o, $@) -c $<
	$(CC) -o $@ -shared $(patsubst %.so, %.o, $@)
	rm $(patsubst %.so, %.o, $@)

clean :
	$(RM) $(OBJ_PATH)/*.o $(OBJ_PATH)/*.so $(OBJ_PATH)/*.a *.exe *.out
