#include "array.h"

int check_prime(int num)
{
    for (int i = 2; i < num; i++)
        if (num % i == 0)
            return 0;
    return 1;
}

int get_next_prime(int current_prime)
{
    do
    {
        current_prime++;
    } while (check_prime(current_prime) == 0);
    return current_prime;
}

void arr_fill_prime(int *arr, int n)
{
    int current_prime = 2;

    for (int i = 0; i < n; i++)
    {
        arr[i] = current_prime;
        current_prime = get_next_prime(current_prime);
    }
}

int arr_form_by_even(int *arr, int len_arr, int *new_arr, int *new_arr_len, int num)
{
    int n = 0;
    for (int i = 0; i < len_arr; i++)
        if (arr[i] % 2 == 0)
            n++;
    n = n + len_arr;

    if (n > *new_arr_len)
    {
        *new_arr_len = n;
        return 1;
    }

    n = 0;
    for (int i = 0; i < len_arr; i++)
    {
        new_arr[n++] = arr[i];
        if (arr[i] % 2 == 0)
            new_arr[n++] = num;
    }
    *new_arr_len = n;
    return 0;
}
